//
//  Vehicle.cpp
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/16/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#include "Vehicle.h"
#include <iostream>
#include <cmath>

using namespace std;


#define PI 3.1415926535898
//conversion factor for degrees to radian
//multiply degree with c to get radian
float c = PI / 180.0;


//default constructor
Vehicle::Vehicle()
{
    mass = 0;
    
    enginePower = 0;
    size = 2;
    
    position = {0,0,0};
    velocity = {0,0,0};
    acceleration = {0,0,0};
    netForce = {0,0,0};
    
    DimensionX = 2.2 * size;
    DimensionY = 0.8 * size;
    DimensionZ = 1.5 * size;
    
    direction = {0, 0, -1};
    yrot = 0;
    
    hoverPower = 0;
    onFloor = false;
}

//constructor with xyz setting
Vehicle::Vehicle(float x, float y, float z)
{
    mass = 0;
    enginePower = 0;
    size = 2;
    
    position = {x,y,z};
    velocity = {0,0,0};
    acceleration = {0,0,0};
    netForce = {0,0,0};
    
    DimensionX = 2.2 * size;
    DimensionY = 0.8 * size;
    DimensionZ = 1.5 * size;
    
    direction = {0, 0, -1};
    yrot = 0;
    
    hoverPower = 0;
    onFloor = false;
}

//set vehicle values
void Vehicle::setStats(float newMass, float newSize, float newEnginePower)
{
    mass = newMass;
    size = newSize;
    enginePower = newEnginePower;
    
    //update ship bounding box
    DimensionX = 2.2 * size;
    DimensionY = 0.8 * size;
    DimensionZ = 1.5 * size;
    

}


//set vehicle force values
void Vehicle::setForce(mVector3 newForce)
{
    netForce = newForce;
}


//debug print out
void Vehicle::displayShipInfo()
{
    cout << "\n***** Vehicle variables *****\n";
    cout << "x_pos: " << position.x <<endl;
    cout << "y_pos: " << position.y <<endl;
    cout << "z_pos: " << position.z <<endl;
    cout << "\n";
    cout << "x_vel: " << velocity.x <<endl;
    cout << "y_vel: " << velocity.y <<endl;
    cout << "z_vel: " << velocity.z <<endl;
    cout << "\n";
    cout << "x_acc: " << acceleration.x <<endl;
    cout << "y_acc: " << acceleration.y <<endl;
    cout << "z_acc: " << acceleration.z <<endl;
    cout << "\n";
    cout << "x_force: " << netForce.x <<endl;
    cout << "y_force: " << netForce.y <<endl;
    cout << "z_force: " << netForce.z <<endl;
}


//update vehicle mass by adding mass, could accept negative numbers
void Vehicle::addShipMass(float addMass)
{
    mass += addMass;
}


//move z pos
void Vehicle::movePosZ()
{
    netForce.z += enginePower;
}

//move z neg
void Vehicle::moveNegZ()
{
    netForce.z += -enginePower;
}

//move x pos RIGHT
void Vehicle::movePosX()
{
    netForce.x += enginePower;
}
//move x neg LEFT
void Vehicle::moveNegX()
{
    netForce.x += -enginePower;
}

//turn ship along y axis, changes direction vector
//theta value in degrees, from positive to negative, determine turn left or right
void Vehicle::turnYAxis(float theta)
{
    mVector3 temp;
    temp.x = direction.x * cos(theta*c) + direction.z * sin(theta*c);
    temp.y = direction.y;
    temp.z = -direction.x * sin(theta*c) + direction.z * cos(theta*c);
    direction = temp;
    yrot += theta;
    if (yrot > 360)
    {
        yrot -= 360;
    }
    else if (yrot < -360)
    {
        yrot += 360;
    }
}

//move forward in direction, applying force in x and z axis without changing y plane
void Vehicle::moveForward()
{
    netForce.x += direction.x * enginePower;
    netForce.z += direction.z * enginePower;
}


//move backward in direction, applying force in multiple axis
void Vehicle::moveBackward()
{
    netForce.x -= direction.x * enginePower;
    netForce.z -= direction.z * enginePower;
}

//move forward in direction, applying force in x and z axis without changing y plane
void Vehicle::moveForward(float power)
{
    netForce.x += direction.x * power;
    netForce.z += direction.z * power;
}


//move backward in direction, applying force in multiple axis
void Vehicle::moveBackward(float power)
{
    netForce.x -= direction.x * power;
    netForce.z -= direction.z * power;
}


//Strafe right, applying force perpendicular to ship direction
void Vehicle::moveStrafeRight()
{
    mVector3 temp;
    temp.x = direction.x * cos(90*c) + direction.z * sin(90*c);
    temp.y = direction.y;
    temp.z = -direction.x * sin(90*c) + direction.z * cos(90*c);
    
    netForce.x -= temp.x * enginePower;
    netForce.z -= temp.z * enginePower;
    
}



//Strafe left, applying force perpendicular to ship direction
void Vehicle::moveStrafeLeft()
{
    mVector3 temp;
    temp.x = direction.x * cos(90*c) + direction.z * sin(90*c);
    temp.y = direction.y;
    temp.z = -direction.x * sin(90*c) + direction.z * cos(90*c);
    
    netForce.x += temp.x * enginePower;
    netForce.z += temp.z * enginePower;
}


//move up
void Vehicle::movePosY()
{
    netForce.y += (enginePower-hoverPower);
}

//move down
void Vehicle::moveNegY()
{
    netForce.y += -(enginePower-hoverPower);
}

//hover mode, lock altitude, tries to set thrust to hover, if not able to then set to max thrust
void Vehicle::enableHoverMode(float gravityCoef)
{
    hoverPower = mass * gravityCoef;
    if (hoverPower > enginePower)
    {
        hoverPower = enginePower;
    }
    
}

//disable hover mode
void Vehicle::disableHoverMode()
{
    hoverPower = 0;
}
