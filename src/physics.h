//
//  physics.h
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 5/10/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#ifndef __Graphics_Game_Prototype__physics__
#define __Graphics_Game_Prototype__physics__

#include <stdio.h>


#include "Levels.h"
#include "Vehicle.h"
//#include "Quadtree.h"


//code pertaining to the physics engine, handling simulations based on ship and its interaction to the environment
//provides collission detection

class PhysicsEngine {
    //friends
    friend void loadLevel(Levels* levelToLoadPtr);
    //**** Game boundaries
    
    //box width (x)
    float x_box;
    //box height (y)
    float y_box;
    //box depth (z)
    float z_box;
    
    //set coundaries based on box size
    float x_half;
    float y_half;
    float z_half;
    
    //**** Enviroment variables
    float gravityCoef;  //normal value 0.004
    float dragCoef;     //normal value 2
    
public:
    void setGameBoundary(float xSize, float ySize, float zSize);
    
    bool wallCollisionBroadTest(Vehicle ship);
    //bool buildingCollision(QuadTree &tree, std::__1::vector<Object> &listOfObjects, Vehicle &ship);
    
    
    
    
    //adds effect of environment forces, gravity, drag
    void applyEnvironmentForces(Vehicle &ship, float gravCoef, float dragCoef);
    
    //update acceleration after net force
    void updateAcceleration(Vehicle &ship);
    
    //updates velocity based on acceleration
    void updateVel(Vehicle &ship);
    
    //translate all forces and move ship, updating ship position based on velocity
    void moveVehicle(Vehicle &ship);
    
    //translate all forces and move dummy ship, updating position based on velocity
    //doesn't do environmental forces
    void moveDummyVehicle(Vehicle &ship);
};





#endif /* defined(__Graphics_Game_Prototype__physics__) */
