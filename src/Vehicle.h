//
//  Vehicle.h
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/16/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#ifndef __Graphics_Game_Prototype__Vehicle__
#define __Graphics_Game_Prototype__Vehicle__

#include <stdio.h>
#include "mVector3.h"

//Vehicle class that stores information about vehicle and provides methods to move it
class Vehicle
{
public:
    
    //friend classes
    friend class Target;
    friend class Camera;
    friend class PhysicsEngine;
    
    //default constructor
    Vehicle();
    
    //constructor with xyz setting
    Vehicle(float x, float y, float z);
    
    //set vehicle values
    void setStats(float newMass, float newSize, float newEnginePower);
    
    //set vehicle force values
    void setForce(mVector3 newForce);
    
    //debug print out
    void displayShipInfo();
    
    
    //update vehicle mass by adding mass, could accept negative numbers
    void addShipMass(float addMass);
    
    
    //draw ship
    friend void drawVehicle(Vehicle vehicleToDraw);
    
    //update third person camera
    friend void moveCamera();
    
    friend void display(void);
    
    
    //*****movement methods
    
    //move z pos Down
    void movePosZ();
    //move z neg UP
    void moveNegZ();
    //move x pos RIGHT strafe
    void movePosX();
    //move x neg LEFT strafe
    void moveNegX();
    //move forward in direction, applying force in multiple axis, with engine power
    void moveForward();
    //move backward in direction, applying force in multiple axis, with engine power
    void moveBackward();
    //move forward in direction, applying force in multiple axis, with given force
    void moveForward(float power);
    //move backward in direction, applying force in multiple axis, with given force
    void moveBackward(float power);
    
    //Strafe right, applying force perpendicular to ship direction
    void moveStrafeRight();
    //Strafe left, applying force perpendicular to ship direction
    void moveStrafeLeft();
    
    //turn ship along y axis, changes direction vector
    //theta value in radians, from positive to negative, determine turn left or right
    void turnYAxis(float theta);
    
    //move up
    void movePosY();

    //move down
    void moveNegY();
    
    //enable hover mode, lock altitude, tries to set thrust to hover, if not able to then set to max thrust
    void enableHoverMode(float gravityCoef);
    
    //disable hover mode
    void disableHoverMode();
    
    
    
private:
    //Private data members
    //vehicle stats
    float size;
    float mass;
    float enginePower;  //how much force does it apply per burst
    float hoverPower;   //how much force is used to hover and maintain altitude. Can't exceed engine power
    
    mVector3 netForce;  //resultant force on vehicle
    
    //something about vehicle dimensions and bounding box
    float DimensionX;
    float DimensionY;
    float DimensionZ;
    
    
    //vehicle movement variables
    //vectors containing xyz info of position, velocity, acceleration
    mVector3 position;
    mVector3 velocity;
    mVector3 acceleration;
    
    mVector3 direction;
    
    //records angle between (0, 0, 1) and rotation of ship in degrees
    float yrot;
    bool onFloor;
    
};


#endif /* defined(__Graphics_Game_Prototype__Vehicle__) */
