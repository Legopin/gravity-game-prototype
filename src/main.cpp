//
//  main.cpp
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/16/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//  now uploaded to bitbucket
//


#include <cmath>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "Levels.h"
#include "Vehicle.h"
#include "Target.h"
//#include "Quadtree.h"
#include "physics.h"

#include "SOIL.h"

using namespace std;


#ifdef __APPLE__
#include <GLUT/glut.h>         /* glut.h includes gl.h and glu.h*/
#else
#include <GL/glut.h>         /* glut.h includes gl.h and glu.h*/
#endif

#define PI 3.1415265359
#define PIdiv180 3.1415265359/180.0

//Debug mode toggle, prints info about ball and wall interactions,
//ball speed during ball freeze, gravity info regarding change in velocity
bool debugMode = false;


//Toggle Variables
bool useFog = true; //fog toggle
bool useLight0 = true;
bool useLight1 = true;

//physics engine
PhysicsEngine physics;

Vehicle *playerPtr;

//Quad tree storing scene
//Quadtree scene(0, 0, 300, 300, 0, 5);





//Global GLUquadricObj object dynamically allocated for drawing GLU quadric objects
GLUquadricObj * ptrToQuadricInfo;


//Global vector to store all levels data
vector<Levels> listOfLevels;

//pointer to current level
Levels *currentLevelPtr;

//GLobal vector to store all textures
vector<GLuint> imageID;

//*****************************************************************
// Begin: Global variables about the camera location and orientation
//*****************************************************************

//Camera on y = 0 plan, circling around (0,0,0)
//The following variable records the current angle between the vector (1,0,0) and (eyex, eyey, eyez)
double C_angle;

//Camera on y = 0 plan, circling around (0,0,0)
//The following variable records the radius of the camera's orbit
double C_Radius;

//Camera on y = 0 plan, circling around (0,0,0)
//The following variable records the rotation speed of the camera
double C_increment;

//Camera on y = 0 plan, circling around (0,0,0)
//Recording the currnet position of the camera.
double eyex, eyey, eyez;

//Camera on y = 0 plan, circling around (0,0,0)
//Specifies the position of the point looked at
double centerx, centery, centerz;


//Specifies the direction of the up vector.
double upx, upy, upz;

//Camera distance, how far it will be behind subject
float cameraDist;

//stores variables regarding camera view
enum class CameraViewType{firstPerson, thirdPerson, aerial};
CameraViewType currentCameraViewType;


class CameraView
{
    double eyexView, eyeyView, eyezView;
    double centerxView, centeryView, centerzView;
    float cameraDistView;
    
    
public:
    CameraViewType viewType;
    
    CameraView()
    {
        eyexView = 0;
        eyeyView = 0;
        eyezView = 0;
        centerxView = 0;
        centeryView = 0;
        centerzView = 0;
        cameraDistView = 0;
        viewType = CameraViewType::thirdPerson;

    }
    CameraView(CameraViewType _viewType, double _eyex, double _eyey, double _eyez, double _centerx, double _centery, double _centerz, float _cameraDist)
    {
        viewType = _viewType;
        eyexView = _eyex;
        eyeyView = _eyey;
        eyezView = _eyez;
        centerxView = _centerx;
        centeryView = _centery;
        centerzView = _centerz;
        cameraDistView = _cameraDist;
        
    }
    void loadCameraView()
    {
        currentCameraViewType = viewType;
        eyex = eyexView;
        eyey = eyeyView;
        eyez = eyezView;
        centerx = centerxView;
        centery = centeryView;
        centerz = centerzView;
        cameraDist = cameraDistView;
    }
    void saveCurrentView()
    {
        viewType = currentCameraViewType;
        eyexView = eyex;
        eyeyView = eyey;
        eyezView = eyez;
        centerxView = centerx;
        centeryView = centery;
        centerzView = centerz;
        cameraDistView = cameraDist;
    }
    
};


//loading views


//Camera views to switch to
CameraView* currentView;
CameraView aerial{CameraViewType::aerial, C_Radius * cos(C_angle), 20, C_Radius * sin(C_angle), 0, -50, 0, 0 };
CameraView firstPerson{CameraViewType::firstPerson, 0, 5, 0, 0, 5, -5, 5};
CameraView thirdPerson; //default view, initilized in init camera settings

//*****************************************************************
// End of Global variables about the camera location and orientation
//*****************************************************************


//*****************************************************************
// Begin: Global variables about lighting
//*****************************************************************

GLfloat ambient[] = {0.5, 0.5, 0.5, 1.0};
GLfloat diffuse[] = {0.8, 0.8, 0.8, 1.0};
GLfloat specular[] = {0.9, 0.9, 0.9, 1.0};
GLfloat position0[] = {0, -50, 0, 1.0};


GLfloat matSpecular[] = {0.01, 0.01, 0.01, 1.0};



//*****************************************************************
// End of global variables about lighting
//*****************************************************************


//*****************************************************************
// Begin: Global variables about vehicles
//*****************************************************************
//vector to store list of vehicles
vector<Vehicle> listOfVehicles;


bool shipActive = true;
bool shipHover = false;
//*****************************************************************
// End of Global variables about vehicles
//*****************************************************************

//*****************************************************************
// Begin: Global variables about Scores and Goals
//*****************************************************************
int gameScore = 0;
int gameCargoAmt = 0;

//scores to pass level
int lvl_01_goal = 5;
int lvl_02_goal = 10;
int lvl_03_goal = 10;



//create goal object
vector<Target> listOfTarget;



//*****************************************************************
// End of Global variables about Scores and Goals
//*****************************************************************


void myShip(float r)
{
    //building ship parts
    glPushMatrix();
    
    //ship overall material
    //grey metal
    GLfloat shipHullMat[] = {0.5, 0.5, 0.5, 1.0};
    GLfloat shipHullSpec[] = {0.9, 0.9, 0.9, 1.0};
    glMaterialf(GL_FRONT, GL_SHININESS, 80);
    glMaterialfv(GL_FRONT, GL_SPECULAR, shipHullSpec);
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, shipHullMat);
    
    //ship main hull center
    glPushMatrix();
    glScalef(r*1.2, r*0.8, r*2);
    
    glLineWidth(1.5);
    glutSolidIcosahedron();
    
    glPopMatrix();
    
    //ship L side part
    glPushMatrix();
    glScalef(r, r*0.6, r*2);
    
    glTranslatef(-0.5 * r, 0, 0.5 *r);
    glutSolidIcosahedron();
    
    glPopMatrix();
    
    //ship R side part
    glPushMatrix();
    glScalef(r, r*0.6, r*2);
    
    glTranslatef(0.5 * r, 0, 0.5 *r);
    glutSolidIcosahedron();
    
    glPopMatrix();
    
     
    glPopMatrix();
}

void mySphere(float x0, float y0, float z0, float r)
{
    glTranslatef(x0, y0, z0);
    GLfloat targetMat[] = {0.9, 0.1, 0.1, 1.0};
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, targetMat);
    GLfloat target_specular[] = {0.9, 0.9, 0.9, 1.0};
    glMaterialfv(GL_FRONT, GL_SPECULAR, target_specular);
    glutSolidSphere(r, 30, 30);
    //glutWireSphere(r, 30, 30);
}

//inits openGL specific settings
void init()
{
    
    /* set up standard orthogonal view with clipping */
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    
    gluPerspective(60, 1, 0.1, 600);
    
    
    /* set clear color to black */
    glClearColor (0.0, 0.0, 0.0, 0.0);
    
    /* set fill  color to white */
    glColor3f(1.0, 1.0, 1.0);
    
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    
    
    
    GLfloat lmodel_ambient[] = {0.4, 0.4, 0.4, 1.0};
    GLfloat local_view[] = {0.0};
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, position0);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    
    
    
    GLfloat specular1[] = {1.0, 1.0, 1.0, 1.0};
    
    
    glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specular1);
    
    
    
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
    
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    
}


void initCameraSetting()
{
    C_angle = 0.5*PI;
    
    //Camera on y = 0 plan, circling around (0,0,0)
    //The following variable records the radius of the camera's orbit
    C_Radius = 130;
    
    //Camera on y = 0 plan, circling around (0,0,0)
    //The following variable records the rotation speed of the camera
    C_increment = 2*PI / (360*2)- 0.001;
    
    
    //Recording the currnet position of the camera.
    eyex = 0; eyey = 0; eyez = 0;
    
    //Specifies the position of the point looked at as (0,0,0).
    centerx = 0; centery = 0; centerz=0;
    
    //Specifies the direction of the up vector.
    upx = 0; upy=1;  upz=0;
    
    //Camera distance, how far it will be behind subject
    cameraDist = 35;//35
    
    //view type
    currentCameraViewType = CameraViewType::thirdPerson;
   
    thirdPerson.saveCurrentView();
    currentView = &thirdPerson;
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyex, eyey, eyez, centerx, centery, centerz, upx, upy, upz);

}


void initGame()
{
    //create vehicle object
    //player vehicle
    Vehicle test;
    test.setStats(100, 2, 12);
    
    listOfVehicles.push_back(test);
    
    playerPtr = &listOfVehicles[0];
    
    //set random target
    Target t1(0,0,0,2,20);
    t1.randomPlace(*currentLevelPtr);
    listOfTarget.push_back(t1);

}


//loads textures from file
void loadTextures()
{
    GLuint tempID;
    
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    //texture 0
    tempID = SOIL_load_OGL_texture("data/PosX.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,SOIL_FLAG_POWER_OF_TWO| SOIL_FLAG_TEXTURE_REPEATS);
    imageID.push_back(tempID);
    
    //texture 1
    tempID = SOIL_load_OGL_texture("data/NegX.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_POWER_OF_TWO| SOIL_FLAG_TEXTURE_REPEATS);
    imageID.push_back(tempID);
    
    //texture 2
    tempID = SOIL_load_OGL_texture("data/PosY.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_POWER_OF_TWO| SOIL_FLAG_TEXTURE_REPEATS);
    imageID.push_back(tempID);
    
    //texture 3
    tempID = SOIL_load_OGL_texture("data/NegY.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_POWER_OF_TWO | SOIL_FLAG_TEXTURE_REPEATS);
    imageID.push_back(tempID);
    
    //texture 4
    tempID = SOIL_load_OGL_texture("data/PosZ.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,SOIL_FLAG_POWER_OF_TWO| SOIL_FLAG_TEXTURE_REPEATS);
    imageID.push_back(tempID);
    
    //texture 5
    tempID = SOIL_load_OGL_texture("data/NegZ.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_POWER_OF_TWO| SOIL_FLAG_TEXTURE_REPEATS);
    imageID.push_back(tempID);
    
    //texture 6
    tempID = SOIL_load_OGL_texture("data/skyScraperWall.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,SOIL_FLAG_POWER_OF_TWO|SOIL_FLAG_MIPMAPS);
    imageID.push_back(tempID);
    
    //texture 7
    tempID = SOIL_load_OGL_texture("data/skyScraperRoof.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,SOIL_FLAG_POWER_OF_TWO|SOIL_FLAG_MIPMAPS);
    imageID.push_back(tempID);
    
}

void loadLevel(Levels* levelToLoadPtr)
{
    //set current level
    currentLevelPtr = levelToLoadPtr;
    
    (*currentLevelPtr).randomizeMapHeight();
    
    useLight0 = currentLevelPtr->light0_On;
    useLight1 = currentLevelPtr->light1_On;
    useFog = currentLevelPtr->fog_On;
    
    //set environment variables
    physics.gravityCoef = currentLevelPtr->gravityCoef;
    physics.dragCoef = currentLevelPtr->dragCoef;
    
    //set game bounding box
    physics.setGameBoundary(currentLevelPtr->x_box, currentLevelPtr->y_box, currentLevelPtr->z_box);
    
}

void moveCamera()
{
    switch (currentCameraViewType)
    {
        case CameraViewType::thirdPerson:
            centerx = playerPtr->position.x;
            centery = playerPtr->position.y;
            centerz = playerPtr->position.z;
            
            eyex = cameraDist * cos((playerPtr->yrot + 270.0f) * PI / 180) + playerPtr->position.x;
            eyez = cameraDist * sin((playerPtr->yrot - 270.0f) * PI / 180) + playerPtr->position.z;
            eyey = cameraDist+playerPtr->position.y-20;
            break;
            
        case CameraViewType::aerial:
            //bird's eye rotation view
            //moves camera around center of map
            //The following variable records the current angle between the vector (1,0,0) and (eyex, eyey, eyez)
            C_angle += C_increment;
            
            if ( C_angle > 2*PI)
                C_angle -= 2*PI;
            
            //rotate around y axis
            eyex = C_Radius * cos(C_angle);
            //eyey = 10;
            eyez = C_Radius * sin(C_angle);
            
            break;
        
        case CameraViewType::firstPerson:
            //yet to be implemented
            centerx = cameraDist * cos((playerPtr->yrot - 270.0f) * PI / 180) + playerPtr->position.x;
            centery = playerPtr->position.y;
            centerz = cameraDist * sin((playerPtr->yrot + 270.0f) * PI / 180) + playerPtr->position.z;
            
            eyex = playerPtr->position.x;
            eyez = playerPtr->position.z;
            eyey = playerPtr->position.y;

            break;
            
        default:
            break;
    }
}


void drawSkyBox(float nHeight, float nWidth, float ndepth)
{
    //height = y, width = x, depth = z
    //GLfloat height = 384, width = 384, depth = 384;
    GLfloat height = nHeight, width = nWidth, depth = ndepth;
    // h, w, d used to simplify vertex assignment
    GLfloat h = height/2, w = width/2, d = depth/2;
    
    GLfloat matSkyBox[] = {0.9, 0.9, 0.9, 1.0};
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, matSkyBox);
    
    glPushMatrix();
    
    
    //glTranslatef(0, -h, 0);
    
    
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    
    //draw Pos X side
    glBindTexture(GL_TEXTURE_2D, imageID[0]);
    
    glBegin(GL_QUADS);
    
    glTexCoord2f(0, 0);     glVertex3f(w, h, -d);
    glTexCoord2f(0, 1);     glVertex3f(w, -h, -d);
    glTexCoord2f(1, 1);     glVertex3f(w, -h, d);
    glTexCoord2f(1, 0);     glVertex3f(w, h, d);
    
    glEnd();
    
    
    //draw Neg X side
    glBindTexture(GL_TEXTURE_2D, imageID[1]);
    
    glBegin(GL_QUADS);
    
    glTexCoord2f(0, 0);     glVertex3f(-w, h, d);
    glTexCoord2f(0, 1);     glVertex3f(-w, -h, d);
    glTexCoord2f(1, 1);     glVertex3f(-w, -h, -d);
    glTexCoord2f(1, 0);     glVertex3f(-w, h, -d);
    
    glEnd();
    
    //draw Pos Y side (sky)
    glBindTexture(GL_TEXTURE_2D, imageID[2]);
    
    glBegin(GL_QUADS);
    
    
    glTexCoord2f(0, 0);     glVertex3f(-w, h, d);
    glTexCoord2f(1, 0);     glVertex3f(-w, h, -d);
    glTexCoord2f(1, 1);     glVertex3f(w, h, -d);
    glTexCoord2f(0, 1);     glVertex3f(w, h, d);
    
    glEnd();
    
    //draw Neg Y side (floor)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glBindTexture(GL_TEXTURE_2D, imageID[3]);
    
    glBegin(GL_QUADS);
    
    
    glTexCoord2f(0, 0);     glVertex3f(-w, -h, -d);
    glTexCoord2f(1, 0);     glVertex3f(-w, -h, d);
    glTexCoord2f(1, 1);     glVertex3f(w, -h, d);
    glTexCoord2f(0, 1);     glVertex3f(w, -h, -d);
    
    glEnd();
    
    
    
    //draw Pos Z side
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glBindTexture(GL_TEXTURE_2D, imageID[4]);
    
    glBegin(GL_QUADS);
    
    glTexCoord2f(0, 0);     glVertex3f(-w, h, d);
    glTexCoord2f(0, 1);     glVertex3f(-w, -h, d);
    glTexCoord2f(1, 1);     glVertex3f(w, -h, d);
    glTexCoord2f(1, 0);     glVertex3f(w, h, d);
    
    glEnd();
    
    //draw Neg Z side
    glBindTexture(GL_TEXTURE_2D, imageID[5]);
    
    glBegin(GL_QUADS);
    
    glTexCoord2f(0, 0);     glVertex3f(-w, h, -d);
    glTexCoord2f(0, 1);     glVertex3f(-w, -h, -d);
    glTexCoord2f(1, 1);     glVertex3f(w, -h, -d);
    glTexCoord2f(1, 0);     glVertex3f(w, h, -d);
    
    glEnd();
    
    glPopMatrix();
    
    glDisable(GL_TEXTURE_2D);
    
}

void drawVehicle(Vehicle vehicleToDraw)
{
    glPushMatrix();
    
    GLfloat position1[] = {0, 0, 0, 1.0};
    GLfloat spotDir[] = {0, 0, -1, 1.0};
    
    glLightfv(GL_LIGHT1, GL_POSITION, position1);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spotDir);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 40);
    
    
    
    myShip(vehicleToDraw.size);
    glPopMatrix();
}

void drawTarget(Target tar)
{
    mySphere(tar.position.x, tar.position.y, tar.position.z, tar.size);
}


/* Copyright (c) Mark J. Kilgard, 1994, 1997. */

/**
 (c) Copyright 1993, Silicon Graphics, Inc.
 
 ALL RIGHTS RESERVED
 
 Permission to use, copy, modify, and distribute this software
 for any purpose and without fee is hereby granted, provided
 that the above copyright notice appear in all copies and that
 both the copyright notice and this permission notice appear in
 supporting documentation, and that the name of Silicon
 Graphics, Inc. not be used in advertising or publicity
 pertaining to distribution of the software without specific,
 written prior permission.
 
 THE MATERIAL EMBODIED ON THIS SOFTWARE IS PROVIDED TO YOU
 "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR
 OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  IN NO
 EVENT SHALL SILICON GRAPHICS, INC.  BE LIABLE TO YOU OR ANYONE
 ELSE FOR ANY DIRECT, SPECIAL, INCIDENTAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER,
 INCLUDING WITHOUT LIMITATION, LOSS OF PROFIT, LOSS OF USE,
 SAVINGS OR REVENUE, OR THE CLAIMS OF THIRD PARTIES, WHETHER OR
 NOT SILICON GRAPHICS, INC.  HAS BEEN ADVISED OF THE POSSIBILITY
 OF SUCH LOSS, HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 ARISING OUT OF OR IN CONNECTION WITH THE POSSESSION, USE OR
 PERFORMANCE OF THIS SOFTWARE.
 
 US Government Users Restricted Rights
 
 Use, duplication, or disclosure by the Government is subject to
 restrictions set forth in FAR 52.227.19(c)(2) or subparagraph
 (c)(1)(ii) of the Rights in Technical Data and Computer
 Software clause at DFARS 252.227-7013 and/or in similar or
 successor clauses in the FAR or the DOD or NASA FAR
 Supplement.  Unpublished-- rights reserved under the copyright
 laws of the United States.  Contractor/manufacturer is Silicon
 Graphics, Inc., 2011 N.  Shoreline Blvd., Mountain View, CA
 94039-7311.
 
 OpenGL(TM) is a trademark of Silicon Graphics, Inc.
 */

static void
drawBox(GLfloat size)
{
    static GLfloat n[6][3] =
    {
        {-1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {1.0, 0.0, 0.0},
        {0.0, -1.0, 0.0},
        {0.0, 0.0, 1.0},
        {0.0, 0.0, -1.0}
    };
    static GLint faces[6][4] =
    {
        {0, 1, 2, 3},
        {3, 2, 6, 7},
        {7, 6, 5, 4},
        {4, 5, 1, 0},
        {5, 6, 2, 1},
        {7, 4, 0, 3}
    };
    GLfloat v[8][3];
    GLint i;
    
    v[0][0] = v[1][0] = v[2][0] = v[3][0] = -size / 2;
    v[4][0] = v[5][0] = v[6][0] = v[7][0] = size / 2;
    v[0][1] = v[1][1] = v[4][1] = v[5][1] = -size / 2;
    v[2][1] = v[3][1] = v[6][1] = v[7][1] = size / 2;
    v[0][2] = v[3][2] = v[4][2] = v[7][2] = -size / 2;
    v[1][2] = v[2][2] = v[5][2] = v[6][2] = size / 2;
    
    for (i = 5; i >= 0; i--)
    {
        
        if (i == 1)
        {
            glBindTexture(GL_TEXTURE_2D, imageID[7]);
            glBegin(GL_QUADS);
            glNormal3fv(&n[i][0]);
            glTexCoord2f(0.0, 0.0); glVertex3fv(&v[faces[i][0]][0]);
            glTexCoord2f(1.0, 0.0); glVertex3fv(&v[faces[i][1]][0]);
            glTexCoord2f(1.0, 1.0); glVertex3fv(&v[faces[i][2]][0]);
            glTexCoord2f(0.0, 1.0); glVertex3fv(&v[faces[i][3]][0]);
        }
        else
        {
            glBindTexture(GL_TEXTURE_2D, imageID[6]);
            glBegin(GL_QUADS);
            glNormal3fv(&n[i][0]);
            
            
            if (i > 2)
            {
                glTexCoord2f(0.0, 1.0); glVertex3fv(&v[faces[i][0]][0]);
                glTexCoord2f(0.0, 0.0); glVertex3fv(&v[faces[i][1]][0]);
                glTexCoord2f(1.0, 0.0); glVertex3fv(&v[faces[i][2]][0]);
                glTexCoord2f(1.0, 1.0); glVertex3fv(&v[faces[i][3]][0]);
            }
            else
            {
                glTexCoord2f(0.0, 0.0); glVertex3fv(&v[faces[i][0]][0]);
                glTexCoord2f(1.0, 0.0); glVertex3fv(&v[faces[i][1]][0]);
                glTexCoord2f(1.0, 1.0); glVertex3fv(&v[faces[i][2]][0]);
                glTexCoord2f(0.0, 1.0); glVertex3fv(&v[faces[i][3]][0]);
            }
        }
        
        
        
        glEnd();
    }
}


static void
drawPartialBox(GLfloat size)
{
    static GLfloat n[6][3] =
    {
        {-1.0, 0.0, 0.0},
        //{0.0, 1.0, 0.0},
        {1.0, 0.0, 0.0},
        //{0.0, -1.0, 0.0},
        {0.0, 0.0, 1.0},
        {0.0, 0.0, -1.0}
    };
    static GLint faces[6][4] =
    {
        {0, 1, 2, 3},
        //{3, 2, 6, 7},
        {7, 6, 5, 4},
        //{4, 5, 1, 0},
        {5, 6, 2, 1},
        {7, 4, 0, 3}
    };
    GLfloat v[8][3];
    GLint i;
    
    v[0][0] = v[1][0] = v[2][0] = v[3][0] = -size / 2;
    v[4][0] = v[5][0] = v[6][0] = v[7][0] = size / 2;
    v[0][1] = v[1][1] = v[4][1] = v[5][1] = -size / 2;
    v[2][1] = v[3][1] = v[6][1] = v[7][1] = size / 2;
    v[0][2] = v[3][2] = v[4][2] = v[7][2] = -size / 2;
    v[1][2] = v[2][2] = v[5][2] = v[6][2] = size / 2;
    
    for (i = 3; i >= 0; i--) {
        glBegin(GL_QUADS);
        glNormal3fv(&n[i][0]);
        
        if (i > 1)
        {
            glTexCoord2f(0.0, 1.0); glVertex3fv(&v[faces[i][0]][0]);
            glTexCoord2f(0.0, 0.0); glVertex3fv(&v[faces[i][1]][0]);
            glTexCoord2f(1.0, 0.0); glVertex3fv(&v[faces[i][2]][0]);
            glTexCoord2f(1.0, 1.0); glVertex3fv(&v[faces[i][3]][0]);
        }
        else
        {
            glTexCoord2f(0.0, 0.0); glVertex3fv(&v[faces[i][0]][0]);
            glTexCoord2f(1.0, 0.0); glVertex3fv(&v[faces[i][1]][0]);
            glTexCoord2f(1.0, 1.0); glVertex3fv(&v[faces[i][2]][0]);
            glTexCoord2f(0.0, 1.0); glVertex3fv(&v[faces[i][3]][0]);
        }
        glEnd();
    }
}

//draw building floor
void drawBuildingFloor(int floorNum, bool partial)
{
    GLfloat matBuilding[] = {0.5, 0.5, 0.5, 1.0};
    
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, matBuilding);
    
    glPushMatrix();
    glTranslatef(0, 20*floorNum+10, 0);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, imageID[6]);
    
    if (partial)
    {
        drawPartialBox(20);
    }
    else
    {
        drawBox(20);
    }
    
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}


//draw building
void drawBuilding(int height, float x, float z)
{
    
    glPushMatrix();
    
    glTranslatef(x, 0, z);
    for (int i = 0; i < height-1; i++)
    {
        drawBuildingFloor(i, true);
    }
    drawBuildingFloor(height-1, false);
    
    glPopMatrix();
}


//draw map layout based on mapGrid array, calling drawBuilding()
//consider putting this in display list to optimize
void drawMapLayout(Levels level)
{
    glPushMatrix();
    
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    
    for (int i = 0; i < 11; i++)
    {
        for (int j = 0; j < 11; j++)
        {
            int height = level.mapLayoutGrid[i][j];
            if (height > 0)
            {
                drawBuilding(height, j*20-80, i*20-80);
            }
        }
    }
    
    glPopMatrix();
}


//print out text to string
//modified from http://stackoverflow.com/questions/2067111/moving-objects-on-screen-by-per-pixel-basis-with-glrasterpos
void renderText(float x, float y, string text) {
    glDisable(GL_LIGHTING);
    int viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(viewport[0], viewport[2], viewport[1], viewport[3], -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glRasterPos2f(x, viewport[3] - y);
    const int length = text.length();
    for (int i = 0; i < length; ++i) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, text[i]);
    }
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();
    glEnable(GL_LIGHTING);
}


void gameStateUpdate()
{
    //vehicle movement
    if (shipActive)
    {
        physics.moveVehicle(*playerPtr);
        
    }
    
    //target update
    if (listOfTarget[0].isCollided(*playerPtr))
    {
        gameScore ++;
        gameCargoAmt++;
        cout << "[Target Acquired!! Score: "<< gameScore << "]\n";
        playerPtr->addShipMass(listOfTarget[0].mass);
        
        if (shipHover)
        {
            playerPtr -> enableHoverMode(currentLevelPtr->getGravityCoef());
        }
        
        listOfTarget.pop_back();
        Target t2(0,0,0,2,20);
        t2.randomPlace(*currentLevelPtr);
        listOfTarget.push_back(t2);
    }

}

void gameCondition()
{
    //advance to next level when score reached, 10 to pass each level, 30 to win game
    
    switch (gameScore)
    {
        case 10:
            loadLevel(&listOfLevels[1]);
            break;
        
        case 20:
            loadLevel(&listOfLevels[2]);
            break;
            
        case 30:
            cout << "***** Congratulations!! You Win!! *****\n";
            cout << "To restart from level 1, press any key and enter\n";
            char temp;
            cin >> temp;
            loadLevel(&listOfLevels[0]);
            gameScore = 0;
            gameCargoAmt = 0;
            break;
        
        default:
            break;
    }
}

void idleDisplay()
{
    //camera update
    moveCamera();
}

void idleFunc()
{
    
    gameStateUpdate();
    idleDisplay();
    gameCondition();
    glutPostRedisplay();
}

void display(void)
{
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    
    gluLookAt(eyex, eyey, eyez, centerx, centery, centerz, upx, upy, upz);
    
    
    /* clear window */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //toggle lights
    if (useLight0)
        glEnable(GL_LIGHT0);
    else
        glDisable(GL_LIGHT0);
    
    if (useLight1)
        glEnable(GL_LIGHT1);
    else
        glDisable(GL_LIGHT1);
    
    //toggle fog
    if (useFog)
    {
        glEnable(GL_FOG);
        {
            GLfloat fogColor[] = {0.66, 0.65, 0.8, 1.0};
            
            //glFogi (GL_FOG_MODE, GL_LINEAR);
            glFogi (GL_FOG_MODE, GL_EXP);
            
            glFogfv (GL_FOG_COLOR, fogColor);
            glFogf (GL_FOG_DENSITY, 0.007);
            glHint (GL_FOG_HINT, GL_DONT_CARE);
            //glFogf (GL_FOG_START, 20.0);
            //glFogf (GL_FOG_END, 110.0);
        }
    }
    else
    {
        glDisable(GL_FOG);
    }
    
    
    drawSkyBox(384, 384, 384);
    
    //draw buildings
    glPushMatrix();
    glTranslatef(0, -192, 0);
    drawMapLayout(*currentLevelPtr);
    
    glPopMatrix();
    
    //draw target
    glPushMatrix();
    drawTarget(listOfTarget[0]);
    glPopMatrix();
    
    //draw other ship
    glPushMatrix();
    
    if (listOfVehicles.size()>1)
    {
        for (int i = 1; i<listOfVehicles.size(); i++)
        {
            glTranslatef(listOfVehicles[i].position.x, listOfVehicles[i].position.y, listOfVehicles[i].position.z);
            myShip(2);
        }

    }
    
    
    glPopMatrix();
    
    
    //draw player ship
    //camera and ship rotation tied together
    glPushMatrix();
    
        glTranslatef(playerPtr->position.x, playerPtr->position.y, playerPtr->position.z);
        glRotatef(playerPtr->yrot, 0, 1, 0);
    
    if (currentCameraViewType != CameraViewType::firstPerson)
    {
        drawVehicle(*playerPtr);
    }
    
    
    glPopMatrix();
    
    //Display textual score and number of cargos
    glColor3f(0.13, 0.92, 0.95);
    string scoreDisplay = "Score: " + to_string(gameScore);
    renderText(50, 50, scoreDisplay);
    
    string cargoDisplay = "Number of Packages: " + to_string(gameCargoAmt);
    renderText(50, 70, cargoDisplay);
    
    
    /* swap and flush GL buffers */
    glutSwapBuffers();
    
}

void keyboard(unsigned char c, int x, int y)
{
    switch (c)
    {
        case 'p':
            //pause simulation and print out ship variables
            shipActive = !shipActive;
            playerPtr->displayShipInfo();
            if (!shipActive)
            {
                cout << "\n[Frozen!]\n";
            }
            break;
            
        case 'w':
            playerPtr->moveForward();
            break;
            
        case 'a':
            playerPtr->turnYAxis(3);
            break;
            
        case 's':
            playerPtr->moveBackward();
            break;
            
        case 'd':
            playerPtr->turnYAxis(-3);
            break;
        
        case 'q':
            playerPtr->moveStrafeLeft();
            break;
            
        case 'e':
            playerPtr->moveStrafeRight();
            break;
            
            
        case ' ':
            //move up
            playerPtr->movePosY();
            break;
        case 'n':
            //move down
            playerPtr->moveNegY();
            break;
        case 'r':
            //random target
            listOfTarget[0].randomPlace(*currentLevelPtr);
            break;
        case 'h':
            //Hover mode, divert engine power to maintain altitude, if unable to maintain then set to engine power
            shipHover = !shipHover;
            if (shipHover)
            {
                playerPtr->enableHoverMode(currentLevelPtr->getGravityCoef());
            }
            else
            {
                playerPtr->disableHoverMode();
            }
            break;
        case 'f':
            //toggle fog effect
            useFog = !useFog;
            break;
            
            
    }
}

void specialKeyboard(int key, int x, int y)
{
    switch (key)
    {
        
        
        default:
            break;
    }
}

void myMenuFunction(int id)
{
    switch (id)
    {
        case 1:
            // Switch to first person view
            if (currentCameraViewType != CameraViewType::firstPerson)
            {
                (*currentView).saveCurrentView();
                currentView = &firstPerson;
                
            }
            break;
        case 2:
            //Switch to third person view
            if (currentCameraViewType != CameraViewType::thirdPerson)
            {
                (*currentView).saveCurrentView();
                currentView = &thirdPerson;
            }
            
            break;
        case 3:
            //Switch to aerial view
            if (currentCameraViewType != CameraViewType::aerial)
            {
                (*currentView).saveCurrentView();
                currentView = &aerial;
            }
            
            break;
            
        default:
            break;
    }
    (*currentView).loadCameraView();
    currentCameraViewType = (*currentView).viewType;
}



int main(int argc, char** argv)
{
    //Print instructions on key control
    
    cout << "r: Set random target\n";
    
    cout << "******* Ship Controls *****\n";
    cout << "w: forward\n";
    cout << "a: turn left\n";
    cout << "s: back\n";
    cout << "d: turn right\n";
    cout << "q: strafe left\n";
    cout << "e: strafe right\n";
    cout << "Space Bar: engine burst up\n";
    cout << "n: engine burst down x\n";
    
    
    /* Initialize mode and open a window in upper left corner of screen */
    
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize (1024, 768);
    glutInitWindowPosition (0, 0);
    
    
    glutCreateWindow("Gravity Game");
    glutDisplayFunc(display);
    glutIdleFunc(idleFunc);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(specialKeyboard);
    
    ptrToQuadricInfo = gluNewQuadric();
    
    //load textures to be used
    loadTextures();
    
    //something about loading level data, including ship info
    
    Levels levelToLoad;
    levelToLoad.readMapFromFile("data/level1.txt");
    listOfLevels.push_back(levelToLoad);
    
    levelToLoad.readMapFromFile("data/level2.txt");
    listOfLevels.push_back(levelToLoad);
    
    levelToLoad.readMapFromFile("data/level3.txt");
    listOfLevels.push_back(levelToLoad);
    
    loadLevel(&listOfLevels[0]);
    
    initGame();

    init();
    initCameraSetting();
    
    //Create menu
    glutCreateMenu(myMenuFunction);
    //Add menu entry
    glutAddMenuEntry("1st Person View", 1);
    glutAddMenuEntry("3rd Person View", 2);
    glutAddMenuEntry("Aerial Overview", 3);
    //Attach menu
    glutAttachMenu(GLUT_RIGHT_BUTTON);
    
    
    glutMainLoop();
    
}
