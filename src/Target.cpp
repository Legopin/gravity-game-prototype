//
//  Target.cpp
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/25/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#include "Target.h"
#include <cstdlib>
#include <ctime>
#include <cmath>

//default constructor
Target::Target()
{
    position = {0,0,0};
    size = 1;
}

//initializing constructor
Target::Target(float x0, float y0, float z0, float newSize, float newMass)
{
    position = {x0, y0, z0};
    size = newSize;
    mass = newMass;
}

//target random position
void Target::randomPlace(Levels level)
{
    //generating target based on map grid
    
    //srand(time(NULL));
    int i = rand()%10;
    int j = rand()%10;
    
    position.x = j*20 - 80;
    position.y = level.mapLayoutGrid[i][j]*20 - 190;
    position.z = i*20 -80;
    
    
    
//    //method of generating random float in range modified from
//    //http://stackoverflow.com/questions/686353/c-random-float-number-generation
//    
//    srand(time(NULL));
//    position.x = lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi-lo)));
//    position.y = lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi-lo)));
//    position.z = lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi-lo)));
}
//collision detection
bool Target::isCollided(Vehicle veh)
{
    //bounding sphere approximation collision detection
    //modified from: http://www.euclideanspace.com/threed/animation/collisiondetect/
    //a is tar, b is veh
    
    //(ax-bx)2+(ay-by)2+(az-bz)2 < (ar+br)2
    
    float xDiffSq = powf((position.x-veh.position.x), 2);
    float yDiffSq = powf((position.y-veh.position.y), 2);
    float zDiffSq = powf((position.z-veh.position.z), 2);
    float rSumSq = powf((size+veh.size*2), 2);
    
    if ( (xDiffSq + yDiffSq + zDiffSq) < rSumSq )
    {
        return true;
    }
    
    return false;
}
