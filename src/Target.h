//
//  Target.h
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/25/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#ifndef __Graphics_Game_Prototype__Target__
#define __Graphics_Game_Prototype__Target__

#include <stdio.h>
#include "mVector3.h"
#include "Vehicle.h"
#include "Levels.h"

class Target {
    mVector3 position;
    float size;
    
    
public:
    float mass;
    
    
    //default constructor
    Target();
    
    //initializing constructor
    Target(float x0, float y0, float z0, float newSize, float newMass);
    
    //target random position, given cube size
    void randomPlace(Levels level);
    
    //collision detection
    bool isCollided(Vehicle veh);
    
    //friend draw class
    friend void drawTarget(Target);
    
    
};

#endif /* defined(__Graphics_Game_Prototype__Target__) */
