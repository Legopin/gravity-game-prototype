//
//  mVector.h
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/23/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#ifndef __Graphics_Game_Prototype__mVector__
#define __Graphics_Game_Prototype__mVector__

#include <stdio.h>

class mVector3
{
    
    
public:
    //public data member
    float x, y, z;
    
    //default constructor
    mVector3();
    
    //constructor given 3 components
    mVector3(float newx, float newy, float newz);
    
    //overloaded assignment operator
    mVector3 & operator =(const mVector3 &rhs);
    
    //overloaded compound addition assignment
    mVector3 & operator+=(const mVector3 &rhs);
    
    //overloaded addition for vector operations on 3 dimensions
    mVector3 operator+(const mVector3 &other);
    
    //overloaded multiplication for vector operation with float scalar value
    mVector3 operator*(float scalar);
    
    //overloaded division for vector operation with floar scalar value
    mVector3 operator/(float scalar);
    
};

#endif /* defined(__Graphics_Game_Prototype__mVector__) */
