//
//  physics.cpp
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 5/10/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#include "physics.h"
#include <iostream>
#include <vector>

using namespace std;

//sets the size of the boundaries for collision detections
void PhysicsEngine::setGameBoundary(float xSize, float ySize, float zSize)
{
    x_box = xSize;
    y_box = ySize;
    z_box = zSize;
    
    x_half = xSize/2;
    y_half = ySize/2;
    z_half = zSize/2;
}

//detects collision of ship with 4 walls, the game boundary
bool PhysicsEngine::wallCollisionBroadTest(Vehicle ship)
{
    //first broad detection, if ship is not within 1/4 of boundary (3/4 from center), assume no collision
    if ((ship.position.x < x_box*0.75 && ship.position.x > -x_box*0.75) &&
        (ship.position.y < y_box*0.75 && ship.position.y > -y_box*0.75) &&
        (ship.position.z < z_box*0.75 && ship.position.z > -z_box*0.75)
        )
    {
        
        float nextXPos = ship.position.x + ship.velocity.x;
        float nextYPos = ship.position.y + ship.velocity.y;
        float nextZPos = ship.position.z + ship.velocity.z;
        if (!(nextYPos < y_half && nextYPos > -y_half))    //y optimized as first item since it happens more often
        {
            return true;
        }
        else if (!(nextXPos < x_half && nextXPos > -x_half))
        {
            return true;
        }
        else if (!(nextZPos < z_half && nextZPos > -z_half))
        {
            return true;
        }
    }
    
    
    return false;
}



//bool PhysicsEngine::buildingCollision(QuadTree &tree, vector<QuadTreeOccupant> &listOfObjects, Vehicle &ship)
//{
//    vector<Object*> nearbyObjects;
//    nearbyObjects = tree.GetObjectsAt(ship.position.x, ship.position.z);
//    
//    
//    //AABB collision method
//    //from: http://www.wildbunny.co.uk/blog/2011/04/20/collision-detection-for-dummies/
//    //D = |centreB-centreA| - (halfExtentsA+halfExtentsB)
//    //binary overlap = D_x < 0 && D_y < 0;
//    // Ship is A, object is B
//    for (int i = 0; i < nearbyObjects.size(); i++)
//    {
//        float left = nearbyObjects[i]->left;
//        float right = nearbyObjects[i]->right;
//        float top = nearbyObjects[i]->top;
//        float down = nearbyObjects[i]->down;
//        
//        float shipLeft  = ship.position.x - ship.DimensionX/2;
//        float shipRight = ship.position.x + ship.DimensionX/2;
//        float shipTop   = ship.position.z - ship.DimensionZ/2;
//        float shipDown  = ship.position.z + ship.DimensionZ/2;
//        
//        float shipBottom = ship.position.y - ship.DimensionY/2;
//        
//        //x axis check
//        if (!(shipLeft > right && shipRight < left))
//        {
//            if (shipBottom < nearbyObjects[i]->height)
//            {
//                cout << "X axis collide at " << ship.position.x << ", " << ship.position.z<<endl;
//                ship.velocity.x *= -0.5;
//                return true;
//            }
//            
//        }
//        //z axis check
//        if (!(shipTop < top && shipDown > down))
//        {
//            if (shipBottom < nearbyObjects[i]->height)
//            {
//                cout << "Z axis collide at " << ship.position.x << ", " << ship.position.z<<endl;
//                ship.velocity.x *= -0.5;
//                return true;
//            }
//        }
//    }
//    
//    
//    return false;
//}



//adds effect of environment forces, gravity, drag
void PhysicsEngine::applyEnvironmentForces(Vehicle &ship, float gravCoef, float dragCoef)
{
    //gravity
    ship.netForce.y -= ship.mass*gravCoef;
    
    //drag
    ship.netForce += (ship.velocity*-dragCoef);
}


//update acceleration after net force
void PhysicsEngine::updateAcceleration(Vehicle &ship)
{
    // a = F/m
    
    ship.acceleration.x = ship.netForce.x/ship.mass;
    ship.acceleration.y = ship.netForce.y/ship.mass;
    ship.acceleration.z = ship.netForce.z/ship.mass;
    
}

//updates velocity based on acceleration
void PhysicsEngine::updateVel(Vehicle &ship)
{
    ship.velocity += ship.acceleration;
}

//translate all forces and move ship, updating ship position based on velocity
void PhysicsEngine::moveVehicle(Vehicle &shipToMove)
{
    applyEnvironmentForces(shipToMove, gravityCoef, dragCoef);
    
    //hover effect
    shipToMove.netForce.y += shipToMove.hoverPower;
    
    
    updateAcceleration(shipToMove);
    updateVel(shipToMove);
    
    //reset netforce to 0
    shipToMove.netForce = {0,0,0};
    
    //collision recovery with walls
    if (wallCollisionBroadTest(shipToMove))
    {
        //then detailed collision detection, with recovery of location
        float nextXPos = shipToMove.position.x + shipToMove.velocity.x;
        if (nextXPos > x_half || nextXPos < -x_half)
        {
            shipToMove.velocity.x *= -0.5;
        }
        
        float nextYPos = shipToMove.position.y + shipToMove.velocity.y - shipToMove.DimensionY;
        if (nextYPos > y_half || nextYPos < -y_half)
        {
            shipToMove.velocity.y *= -0.5;
        }
        
        float nextZPos = shipToMove.position.z + shipToMove.velocity.z;
        if (nextZPos > z_half || nextZPos < -z_half)
        {
            shipToMove.velocity.z *= -0.5;
        }
    }
    //http://www.wildbunny.co.uk/blog/2011/04/20/collision-detection-for-dummies/
    //D = |centreB-centreA| - (halfExtentsA+halfExtentsB)
    //binary overlap = D_x < 0 && D_y < 0;
    
    shipToMove.position += shipToMove.velocity;
}


//translate all forces and move dummy ship, updating position based on velocity
//doesn't do environmental forces
void PhysicsEngine::moveDummyVehicle(Vehicle &ship)
{
    updateAcceleration(ship);
    updateVel(ship);
    
    //reset netforce to 0
    ship.netForce = {0,0,0};
    
    ship.position += ship.velocity;
}

