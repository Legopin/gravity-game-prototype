//
//  Levels.h
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/21/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

//Contains information about each level and the corresponding environment variables
//including gravity, density of air (drag), map geometry (buildings)

#ifndef __Graphics_Game_Prototype__Levels__
#define __Graphics_Game_Prototype__Levels__

#include <stdio.h>

class Levels
{
    friend class Vehicle;
    friend class Target;
    friend void loadLevel(Levels* levelToLoadPtr);
private:
    //**** Game boundaries
    
    //box width (x)
    float x_box;
    //box height (y)
    float y_box;
    //box depth (z)
    float z_box;
    
    //environment variables
    float gravityCoef;
    float dragCoef;
    bool light0_On; //environment light
    bool light1_On; //spot light
    bool fog_On;    //environment fog
    
    //map layout grid array
    int mapLayoutGrid[11][11];
    
    
    
public:
    
    
    
    //reads level layout data from txt file
    void readMapFromFile(char fileName[]);
    
    //randomize height of buildings on map
    void randomizeMapHeight();
    
    //get level gravity
    float getGravityCoef();
    
    
    friend void drawMapLayout(Levels level);
    friend void loadLevel(Levels levelToLoad);
    
};


#endif /* defined(__Graphics_Game_Prototype__Levels__) */
