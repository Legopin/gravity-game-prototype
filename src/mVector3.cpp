//
//  mVector.cpp
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/23/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

#include "mVector3.h"

//default constructor
mVector3::mVector3()
{
    x = y = z = 0;
}

//constructor given 3 components
mVector3::mVector3(float newx, float newy, float newz)
{
    x = newx;
    y = newy;
    z = newz;
}


//overloaded addition for vector operations on 3 dimensions
mVector3 mVector3::operator+(const mVector3 &rhs)
{
    mVector3 result;
    result.x += rhs.x;
    result.y += rhs.y;
    result.z += rhs.z;
    
    return result;
}

//overloaded assignment operator
mVector3 & mVector3::operator =(const mVector3 &rhs)
{
    x = rhs.x;
    y = rhs.y;
    z = rhs.z;
    
    return *this;
}


//overloaded compound addition assignment
mVector3 & mVector3::operator+=(const mVector3 &rhs)
{
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    
    return *this;
}


//overloaded multiplication for vector operation with float scalar value
mVector3 mVector3::operator*(float scalar)
{
    mVector3 result = *this;
    
    result.x *= scalar;
    result.y *= scalar;
    result.z *= scalar;
    
    return result;
}

//overloaded division for vector operation with floar scalar value
mVector3 mVector3::operator/(float scalar)
{
    mVector3 result;
    
    result.x /= scalar;
    result.y /= scalar;
    result.z /= scalar;
    
    return result;
}