//
//  Levels.cpp
//  Graphics Game Prototype
//
//  Created by Sean Yuan on 3/21/15.
//  Copyright (c) 2015 Sean Yuan. All rights reserved.
//

//Contains information about each level and the corresponding environment variables
//including gravity, density of air (drag), map geometry (buildings)

#include "Levels.h"

#include <ctime>
#include <random>
#include <fstream>
#include <iostream>

using namespace::std;

//reads level layout data from txt file
//last line [12] contains these variables: x_box, y_box, z_box, gravityCoef, dragCoef
void Levels::readMapFromFile(char fileName[])
{
    
    ifstream inputFile;     //File object
    
    
    inputFile.open(fileName);   //Open file
    
    if (inputFile)
    {
        
        
        for (int i = 0; i < 11; i++)   //Read map layout into array, row
        {
            for (int j=0; j < 11; j++)
            {
                inputFile >> mapLayoutGrid[i][j];
            }
        }
        
        //read enviromental variables into level
        inputFile >> x_box;
        inputFile >> y_box;
        inputFile >> z_box;
        inputFile >> gravityCoef;
        inputFile >> dragCoef;
        inputFile >> light0_On; //0 or 1 read to bool
        inputFile >> light1_On; //0 or 1 read to bool
        inputFile >> fog_On;    //0 or 1 read to bool
        
        
        inputFile.close();  //Close file
        
        cout << "map level layout successfully read from file! \n";   //Confirmation message
        
    }
    else
    {
        cout << "Error! Can't open the file \n";    //Error message
    }
    
}

//randomize the height of all buildings on the map, within the range of 1-7 integer
void Levels::randomizeMapHeight()
{
    srand(time(NULL));
    for (int i = 0; i < 11; i++)
    {
        for (int j=0; j < 11; j++)
        {
            if (mapLayoutGrid[i][j] == 1)
            {
                mapLayoutGrid[i][j] = rand()%7+1;
            }
        }
    }
}

//get level gravity
float Levels::getGravityCoef()
{
    return gravityCoef;
}