![GravityWeb.jpg](https://bitbucket.org/repo/eMobp6/images/1480688679-GravityWeb.jpg)


### What is this repository for? ###
Gravity is a 3D game built with C++ and OpenGL for the course CSCI 440 Computer Graphics. The player operates a space delivery vehicle and navigates the 3D randomly generated level collecting cargo. The game has been compiled for both Windows and Mac.

### Screenshots ###
![screenshot3Web.jpg](https://bitbucket.org/repo/eMobp6/images/2451569992-screenshot3Web.jpg)

### How to play? ###

* Download the [Windows version](https://dl.dropboxusercontent.com/u/31595579/GravityGameWin.zip) / 
* Unzip the folder and click "Gravity Game.ext" to play
* Consult the User Manual for details on how to play the game

### How to create your own level? ###
The levels are easily modified through the use of text files within the "data" folder. The levels are represented by a grid of 1's and 0's denoting whether there is a building or not. 

The bottom row contains the environment variables for the specific level in the following order

+ Gravity coefficient
+ Drag coefficient
+ Day or Night (0 for night, 1 for day)
+ Headlight default on (0 or 1)
+ Enable/ disable Fog (0 or 1)

### Code segments used ###

* Bounding sphere approximation collision detection
    * http://www.euclideanspace.com/threed/animation/collisiondetect/
* SOIL (Simple Opengl Image Library) by Jonathan Dummer
    * http://www.lonesock.net/soil.html
* GLUT SolidCube Source Code by Mark J. Kilg
    * Glut source code

### Acknowledgements ###
* Christopher Hilderbrand for consulting with mathematical implementation of direction vector movement
* Testers: Diana, Hezki, Edlyn
* Logo design by: Diana Darmawan